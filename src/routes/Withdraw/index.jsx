import Container from './styles'
import {Test} from './styles'
import {useEffect, useState} from 'react';

const Withdraw = () => {
    const [boo, setBoo] = useState(true);
    const [cpfValue, setCpf] = useState("")
    const [ValueWithdraw, setValue] = useState(0)
    const [errorsV, setErros] = useState('')
    const [data, setData] = useState('')

    const post = () => {
        fetch('http://localhost:3000/api/withdraw', requestOptions)
            .then(response => response.json())
            .then(data => setData(data))}

    useEffect(() => {
        if(cpfValue !== "" && ValueWithdraw > 0 && cpfValue.length === 11){
        post()
        setErros('')

        }else if(boo !== true){
            setErros('Necessário CPF e Values Válidos')
        }
    }, [boo]);
        
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ cpf: cpfValue, value: parseInt(ValueWithdraw) })
    };

    return (
        <Container>
            <Container.Title >
                Withdraw
            </Container.Title >
            <Container.Inputs>

            <Test 
            id="filled-basic"
            label="CPF"
            variant="filled"
            value={cpfValue}
            onChange={(e)=>setCpf(e.target.value)}/>
            <Test 
            type="number"
            id="filled-basic"
            label="Value"
            variant="filled"
            value={ValueWithdraw}
            onChange={(e)=>setValue(e.target.value)}/>

            </Container.Inputs>
            <Container.Inputs>

            {errorsV != '' ? <Container.Error>{errorsV}</Container.Error> : <></>}
            </Container.Inputs>

            <Container.Inputs>
                <Container.Button  onClick={()=>setBoo(!boo)}>
                    Send
                </Container.Button >
            </Container.Inputs>
            <Container.Card>
            <Container.Title>
                Results:
            </Container.Title>
            {data? <Container.Map>
                <div>
                Balance: {data.balance}
                </div>
                <div>
                Cpf: {data.cpf}
                </div>
                <div>
                Value: {data.value}
                </div>
            </Container.Map> : <></>}
            </Container.Card>
        </Container>
    )
} 
export default Withdraw;