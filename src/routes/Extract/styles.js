import styled from 'styled-components'
import {TextField} from '@material-ui/core/';


const Container = styled.div`
    width: 100%;
    height: 100%;

    background-color: #f4efa6;

    border-radius: 14px;
`;
Container.Title = styled.div`
    font-family: Poppins, sans-serif;
    font-weight: 400;

    padding: 10px 0 0 13px;

    color: black;
    font-size: 25px;
`;
Container.Inputs = styled.div`
    width: calc(90%);

    padding: 15px;
    display: flex;
    justify-content: space-evenly;
`;
export const Test = styled(TextField)`
    margin: 15px;
    >div{
        width: 20rem;}
        `;
Container.Error = styled.div`
    color: red;
    font-size: 15px;
`;
Container.Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    
    width: 6.5em;
    height: 2em;
    border-radius: 4px;
    padding: 0 5px;

    background-color: #edc531;
    &:hover{
        background-color: #fad643;
        border: 1px solid white;
        width: calc(6.5em - 2px);
        height: calc(2em - 2px);
    }
    transition: background 0.2s;
    font-size: 25px;

    font-family: Poppins, sans-serif;
    font-weight: 600;
    cursor: pointer;
`;
Container.Card = styled.div`
    font-size: 25px;

    display: flex;
    flex-flow: column;
    @media(min-width: 800px) {
        height: calc(25rem + 2px);
        overflow: auto;
    }
    width: 100%;
    height: 20rem;

    overflow: scroll;
    background-color: #fad643;

    border-radius: 14px;
`;
Container.Header = styled.div`
    display: flex;
    justify-content:space-around; 

    padding: 0 0 30px 15px ;

    width: 90%;
    height: 1.5em;
    
    font-size: 25px;

    font-family: Poppins, sans-serif;
    font-weight: 600;
`;
Container.Map = styled.div`
    border: 1px solid white;
    
    padding-left: 25px;

    display: flex;
    flex-flow: column;
    justify-content: flex-start;
    width: 90%;
    height: 3em;
    
    margin-left: 0.5em;
    @media(max-width: 800px) {
        margin-left: 0.2em;

    }
    font-size: 25px;

    font-family: Poppins, sans-serif;
    font-weight: 300;
`;
Container.Extract = styled.div`
    display: flex;
    justify-content: center;
    flex-flow: row wrap;
    align-items: center;

    color: black;`;
export default Container