import Container from './styles'
import {Test} from './styles'
import {useEffect, useState} from 'react';

const Extract = () => {
    const [boo, setBoo] = useState(true);
    const [cpfValue, setCpf] = useState("")
    const [errorsV, setErros] = useState('')
    const [data, setData] = useState('')

    const extract = () => {
        fetch('http://localhost:3000/api/extract', requestOptions)
            .then(response => response.json())
            .then(res => setData(res))}
    useEffect(() => {
        if(boo !== true){
            if(cpfValue.length === 11){
                extract()
                console.log("Extrato!")
            }else{
                setErros('Necessário um CPF Válido')
            }
        }}, [boo]);
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ cpf: cpfValue})
        };

    return (
        <Container>
            <Container.Title >
                Extract
            </Container.Title >
            <Container.Inputs>

            <Test 
            id="filled-basic"
            label="CPF"
            variant="filled"
            value={cpfValue}
            onChange={(e)=>setCpf(e.target.value)}/>
            </Container.Inputs>
            <Container.Inputs>

            {errorsV !== '' ? <Container.Error>{errorsV}</Container.Error> : <></>}
            </Container.Inputs>

            <Container.Inputs>
                <Container.Button  onClick={()=>setBoo(!boo)}>
                    Send
                </Container.Button >
            </Container.Inputs>
            <Container.Card>
            <Container.Title>
                Results:
            </Container.Title>
            <Container.Header>
                <div>
                CPF: {data.balance?.cpf}
                </div>
                <div>
                Balance: {data.balance?.balance}
                </div>
            </Container.Header>
                {data.activities?.map((element, index)=>(
                    <Container.Map key={index}>
                        <div>
                        Value: {element.balance}
                        </div>
                        <div>
                            Time: {element.date.split('T').join(' ').split('Z')}
                        </div>
                    </Container.Map>
                ))}
            </Container.Card>
        </Container>
    )
} 
export default Extract;