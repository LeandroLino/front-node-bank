import Container from './styles'
import {Test} from './styles'
import {useEffect, useState} from 'react';

const Withdraw = () => {
    const [boo, setBoo] = useState(true);
    const [ToCpfValue, setToCpf] = useState("")
    const [FromCpfValue, setFromCpf] = useState("")
    const [ValueTransfer, setValue] = useState(0)
    const [errorsV, setErros] = useState('')
    const [data, setData] = useState('')

    const post = () => {
        fetch('http://localhost:3000/api/transfer', requestOptions)
            .then(response => response.json())
            .then(data => setData(data))}

    useEffect(() => {
        if(ToCpfValue !== FromCpfValue){
        if(ToCpfValue !== "" && FromCpfValue !== "" &&  ValueTransfer > 0 && ToCpfValue.length === 11 && 
        FromCpfValue.length === 11){
        post()
        setErros('')
        }else if(boo !== true){
            setErros('Necessário CPF e Values Válidos')
        }
    }else if(boo !== true){
        setErros('Os Cpfs precisam ser diferentes!')
    }
    }, [boo]);
        
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ to_cpf: ToCpfValue, from_cpf: FromCpfValue,value: parseInt(ValueTransfer) })
    };

    return (
        <Container>
            <Container.Title >
                Transfer
            </Container.Title >
            <Container.Inputs>
            <Test 
            id="filled-basic"
            label="From CPF"
            variant="filled"
            value={FromCpfValue}
            onChange={(e)=>setFromCpf(e.target.value)}/>
            <Test 
            type="number"
            id="filled-basic"
            label="Value"
            variant="filled"
            value={ValueTransfer}
            onChange={(e)=>setValue(e.target.value)}/>
            <Test 
            id="filled-basic"
            label="To CPF"
            variant="filled"
            value={ToCpfValue}
            onChange={(e)=>setToCpf(e.target.value)}/>
            </Container.Inputs>

            <Container.Inputs>

            {errorsV != '' ? <Container.Error>{errorsV}</Container.Error> : <></>}
            </Container.Inputs>

            <Container.Inputs>
                <Container.Button  onClick={()=>setBoo(!boo)}>
                    Send
                </Container.Button >
            </Container.Inputs>
            <Container.Card>
            <Container.Title>
                Results:
            </Container.Title>
            {console.log(data)}
            {data[0]? <Container.Map>
                <div>
                Balance: {data[0].balance}
                </div>
                <div>
                To Cpf: {data[0].cpf}
                </div>
                <div>
                Value: {data[0].value}
                </div>
            </Container.Map> : <></>}
            {data[1]? <Container.Map>
                <div>
                Balance: {data[1].balance}
                </div>
                <div>
                From Cpf: {data[1].cpf}
                </div>
                <div>
                Value: {data[1].value}
                </div>
            </Container.Map> : <></>}
            </Container.Card>
        </Container>
    )
} 
export default Withdraw;