import styled from 'styled-components'
import {TextField} from '@material-ui/core/';


const Container = styled.div`
    width: 100%;
    height: 100%;

    background-color: #f4efa6;

    border-radius: 14px;
`;
Container.Title = styled.div`
    font-family: Poppins, sans-serif;
    font-weight: 400;

    padding: 10px 0 0 13px;

    color: black;
    font-size: 25px;
`;
Container.Inputs = styled.div`
    width: 90%;
    margin: 0;
    padding: 15px;
    padding-left: 25px;

    @media(max-width: 800px) {
        flex-flow: row wrap;
    }

    display: flex;
    justify-content: space-around;
`;
export const Test = styled(TextField)`
    margin: 15px;
    >div{
        width: 10rem;}
`;
Container.Error = styled.div`
    color: red;
    font-size: 15px;
    padding: 0;
`;
Container.Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    
    width: 6.5em;
    height: 2em;
    border-radius: 4px;
    padding: 0 5px;

    background-color: #edc531;
    &:hover{
        background-color: #fad643;
        border: 1px solid white;
        width: calc(6.5em - 2px);
        height: calc(2em - 2px);
    }
    transition: background 0.2s;
    font-size: 25px;

    font-family: Poppins, sans-serif;
    font-weight: 600;
    cursor: pointer;
`;
Container.Map = styled.div`
    display: flex;
    flex-flow: column;
    justify-content: flex-start;
    width: 90%;
    height: 3em;
    
    margin-left: 5px;
    >:first-child{
        margin-top: 0;
    }
        margin-top: 25px;

    font-size: 22px;

    font-family: Poppins, sans-serif;
    font-weight: 300;
`;
Container.Card = styled.div`
    font-size: 25px;

    display: flex;
    flex-flow: column;
    @media(max-width: 800px) {
        height: calc(16.5rem + 3px);
    }
    width: 100%;
    height: calc(25rem);

    overflow: hidden;
    background-color: #fad643;

    border-radius: 14px;
`;

export default Container