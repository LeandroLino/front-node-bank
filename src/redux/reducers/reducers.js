import { ACTION_TYPES } from "../actions/action-types";

const defaultState = {
  /* default State goes here */
}

const reducer = (state = defaultState, action) => {
  switch(action.type) {
        
    /* cases for the switch */

    default: 
      return state;
    }
}

export default reducer;