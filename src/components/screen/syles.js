import styled from 'styled-components'


const Container = styled.div`
    width: 100%;
    height: 100vh;
    background: rgb(244,239,166);
    background: linear-gradient(214deg, rgba(244,239,166,1) 0%, rgba(255,255,255,1) 100%);
    //background-color:#A4FBF6;

    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: row wrap;

    `;
Container.Screen = styled.div`
border-radius: 14px;
    width: 60%;
    height: 80vh;
    background-color: white;
    @media(max-width: 800px) {
        width: 90%;
    }
`;
Container.Bar = styled.div`
    @media(max-width: 800px) {
        width: 90%;
        height: 13vh
    }
    @media(min-width: 800px) {
        width: 10em;
        height: 80vh;
        flex-flow: column;
        justify-content: center;

    }
    /*width: 90%;
    height: 13vh;
*/
    display: flex;
    justify-content: space-around;
    align-items: center;
`;
Container.Icon= styled.div`
    @media(min-width: 800px) {
        &:hover{
            width: calc(9em - 2px);
            height: calc(18vh - 2px);
        }

        width: 9em;
        height: 18vh;
        margin-bottom: 10px;    
    }
    width: 25%;
    height: 100%;
    
    margin: 5px;
    
    &:hover{
        background-color: #f4efa6;
        border: 1px solid white;
    }
    background-color:#fad643;
    transition: background 0.5s;
    //background-color: #80ffdb;
    border-radius: 14px;
    display: flex;
    justify-content: center;
    align-items: center;
    &:hover{
        > img{
        width: calc(81%);
        height: calc(81%);
    }
        }
    > img{
        width: 80%;
        height: 80%;
    }
`;

export default Container