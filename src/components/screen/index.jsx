import {useEffect} from 'react';
import Container from './syles'
import deposit from '../../assets/deposit.svg'
import withdraw from '../../assets/withdraw.svg'
import extract from '../../assets/extract.svg'
import transfer from '../../assets/transfer.svg'
import { useHistory, Route, Switch } from "react-router-dom";
import DepositRoute from '../../routes/Deposit'
import WithdrawRoute from '../../routes/Withdraw'
import ExtractRoute from '../../routes/Extract'
import TransferRoute from '../../routes/Transfer'

const Screen = () => {
    const history = useHistory()
    useEffect(() => {
    },[]);
    return (
    <Container>
        <Container.Bar>
            <Container.Icon onClick={()=>{history.push('/')}}  >
                <img src={deposit}></img>
            </Container.Icon>

            <Container.Icon>
                <img src={withdraw} onClick={()=>{history.push('/withdraw')}}></img>
            </Container.Icon>
            <Container.Icon>
                <img src={extract} onClick={()=>{history.push('/extract')}}></img>
            </Container.Icon>
            <Container.Icon>
                <img src={transfer} onClick={()=>{history.push('/transfer')}}></img>
            </Container.Icon>
        </Container.Bar>
        <Container.Screen>
            <Switch>
                <Route exact path="/">
                    <DepositRoute/>
                </Route>
                <Route exact path="/withdraw">
                    <WithdrawRoute />
                </Route>
                <Route exact path="/extract" >
                    <ExtractRoute />
                </Route>
                <Route exact path="/extract" >
                    <ExtractRoute />
                </Route>
                <Route exact path="/transfer" >
                    <TransferRoute />
                </Route>
            </Switch>
        </Container.Screen>
    </Container>
    );
}

export default Screen;
