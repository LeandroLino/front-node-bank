import React from 'react';
import './App.css';
import Screen from './components/screen'
const App = () => {
  return (
    <div>
      <Screen/>
    </div>
  );
}

export default App;
